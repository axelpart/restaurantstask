# Networking

An HTTP network package. Use `NetworkServiceFactory` to create an instance of the NetworkService.

Currently the following models are used:
- EndPoint
- NetworkServiceType
- RequestType
