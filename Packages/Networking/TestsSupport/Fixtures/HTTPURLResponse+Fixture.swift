import Foundation

extension HTTPURLResponse {
    static func fixture(
        url: URL = URL(fileURLWithPath: ""),
        statusCode: Int = 0,
        httpVersion: String? = nil,
        headerFields: [String: String]? = nil
    ) -> HTTPURLResponse? {
        HTTPURLResponse(
            url: url,
            statusCode: statusCode,
            httpVersion: httpVersion,
            headerFields: headerFields
        )
    }
}
