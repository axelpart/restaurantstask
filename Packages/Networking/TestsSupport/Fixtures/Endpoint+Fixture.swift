import Foundation
import Networking

extension Endpoint {
    static func fixture(
        host: String = "",
        path: String = "",
        queryItems: [URLQueryItem] = []
    ) -> Endpoint {
        Endpoint(
            host: host,
            path: path,
            queryItems: queryItems
        )
    }
}
