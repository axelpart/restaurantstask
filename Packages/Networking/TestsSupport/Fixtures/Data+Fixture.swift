import Foundation

extension Data {
    static func fixture(string: String = "") -> Data {
        Data(string.utf8)
    }
}
