import Foundation

extension URL {
    static func fixture(string: String = "") -> URL? {
        URL(string: string)
    }
}
