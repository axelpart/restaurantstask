import Foundation
import Networking

struct RequestTypeMock: RequestType {
    let endpoint: Endpoint
    let httpMethod: HTTPMethod

    init(endpoint: Endpoint = .fixture(), httpMethod: HTTPMethod = .get) {
        self.endpoint = endpoint
        self.httpMethod = httpMethod
    }
}
