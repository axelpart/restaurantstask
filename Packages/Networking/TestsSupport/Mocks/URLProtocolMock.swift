import Foundation
@testable import Networking

final class URLProtocolMock: URLProtocol {
    static var underlyingURLData = [URL?: Data]()
    static var underlyingResponse: HTTPURLResponse?
    static var underlyingError: Error?
    static var requestReceivedInvocations = [URLRequest]()

    override class func canInit(with request: URLRequest) -> Bool {
        Self.requestReceivedInvocations.append(request)
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        request
    }

    override func startLoading() {
        if let url = request.url, let data = Self.underlyingURLData[url] {
            client?.urlProtocol(self, didLoad: data)
        }

        if let response = Self.underlyingResponse {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        }

        if let error = Self.underlyingError {
            let err = (error as NSError)
            client?.urlProtocol(self, didFailWithError: err)
        }

        client?.urlProtocolDidFinishLoading(self)
    }

    override class func requestIsCacheEquivalent(_ a: URLRequest, to b: URLRequest) -> Bool {
        false
    }

    override func stopLoading() { }
}
