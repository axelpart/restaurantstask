import Foundation
import Networking

final public class NetworkServiceTypeMock: NetworkServiceType {

    public init() { }

    public var requestResponseTypeThrowableError: Error?
    public var requestResponseTypeCallsCount = 0
    public var requestResponseTypeCalled: Bool {
        return requestResponseTypeCallsCount > 0
    }

    public var requestResponseTypeReturnValue: Any!

    public func request<T: Decodable>(_ request: RequestType, responseType: T.Type) async throws -> T {
        if let error = requestResponseTypeThrowableError {
            throw error
        }
        requestResponseTypeCallsCount += 1
        return requestResponseTypeReturnValue as! T
    }
}
