import Foundation
import Networking

final class JSONDecoderTypeMock: JSONDecoderType {
    var decodeThrowableError: Error?
    var decodeCallsCount = 0
    var decodeCalled: Bool {
        return decodeCallsCount > 0
    }
    var decodeReturnValue: Any!

    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T: Decodable {
        if let error = decodeThrowableError {
            throw error
        }
        decodeCallsCount += 1
        return decodeReturnValue as! T
    }
}
