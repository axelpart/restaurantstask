import XCTest
@testable import NetworkingTestsSupport
@testable import Networking

final class NetworkServiceTests: XCTestCase {
    var urlSessionMock: URLSession!
    var decoderMock: JSONDecoderTypeMock!
    var sut: NetworkService!

    var configuration: URLSessionConfiguration!
    var urlTestData: [URL?: Data]!
    var testRequest: RequestTypeMock!

    // MARK: - Life cycle

    override func setUpWithError() throws {
        try super.setUpWithError()

        configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [URLProtocolMock.self]
        urlTestData = [.fixture(string: "https://www.test.com"): .fixture(string: "any")]
        testRequest = RequestTypeMock(endpoint: .fixture(host: "www.test.com"))
        urlSessionMock = URLSession(configuration: configuration)
        decoderMock = JSONDecoderTypeMock()
        sut = NetworkService(
            urlSession: urlSessionMock,
            decoder: decoderMock
        )
    }

    override func tearDownWithError() throws {
        URLProtocolMock.underlyingURLData = [:]
        URLProtocolMock.requestReceivedInvocations = []
        URLProtocolMock.underlyingResponse = nil

        configuration = nil
        urlTestData = nil
        testRequest = nil
        urlSessionMock = nil
        decoderMock = nil
        sut = nil
        try super.tearDownWithError()
    }

    // MARK: - Decoding

    func testRequestSucceedsWithDecodableResponse() async throws {
        // Given
        given(
            decodeReturnValue: DecodableItem(id: "id"),
            response: .fixture(statusCode: 200)
        )

        // When
        let result = try await sut.request(testRequest, responseType: DecodableItem.self)

        // Then
        XCTAssertEqual(result.id, "id")
    }

    func testRequestWhenDecodingFails() async throws {
        // Given
        var expectedResult: NetworkError?
        given(
            decodeThrowableError: ErrorFixture.fake,
            response: .fixture(statusCode: 200)
        )

        // When
        do {
            _ = try await sut.request(testRequest, responseType: DecodableItem.self)
        } catch {
            expectedResult = error as? NetworkError
        }

        // Then
        XCTAssertEqual(expectedResult, .decodingError)
    }

    // MARK: - Errors

    func testRequestWhenURLIsNotValid() async throws {
        // Given
        let request = RequestTypeMock(endpoint: .fixture(path: "#$"))
        var expectedResult: NetworkError?

        // When
        do {
            _ = try await sut.request(request, responseType: DecodableItem.self)
        } catch {
            expectedResult = error as? NetworkError
        }

        // Then
        XCTAssertEqual(expectedResult, .invalidURL)
    }

    func testRequestWhenResponseStatusCodeIsNot200() async throws {
        // Given
        var expectedResult: NetworkError?
        given(response: .fixture(statusCode: 400))

        // When
        do {
            _ = try await sut.request(testRequest, responseType: DecodableItem.self)
        } catch {
            expectedResult = error as? NetworkError
        }

        // Then
        XCTAssertEqual(expectedResult, .invalidResponse)
    }

    // MARK: - Private helper

    private func given(
        decodeReturnValue: Any? = nil,
        decodeThrowableError: Error? = nil,
        underlyingURLData: [URL?: Data] = [:],
        response: HTTPURLResponse? = nil
    ) {
        decoderMock.decodeReturnValue = decodeReturnValue
        decoderMock.decodeThrowableError = decodeThrowableError
        URLProtocolMock.underlyingURLData = underlyingURLData
        URLProtocolMock.underlyingResponse = response
    }
}
