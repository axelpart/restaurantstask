import XCTest
@testable import Networking

final class EndpointTests: XCTestCase {
    func testEndpointURLWithEmptyURLComponents() {
        // When
        let endpoint = Endpoint(
            host: "test",
            path: "/api",
            queryItems: []
        )

        // Then
        XCTAssertEqual(
            endpoint.url,
            URL(string: "https://test/api")
        )
    }

    func testEndpointReturnWithURLComponents() {
        // When
        let endpoint = Endpoint(
            host: "test",
            path: "/api",
            queryItems: [URLQueryItem(name: "item", value: "1")]
        )

        // Then
        XCTAssertEqual(
            endpoint.url,
            URL(string: "https://test/api?item=1")
        )
    }
}
