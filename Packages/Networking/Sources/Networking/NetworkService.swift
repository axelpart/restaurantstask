import Foundation

public protocol NetworkServiceType {
    /// Performs a request with a `Decodable` type from response data.
    ///
    /// - Parameters:
    ///   - endpoint: An `Endpoint` value to be used to create the `URLRequest`.
    ///   - responseType: Any generic type that implements `Decodable` protocol.
    ///
    /// - Returns: A decoded object.
    /// - Throws: A `NetworkError`
    func request<T: Decodable>(_ request: RequestType, responseType: T.Type) async throws -> T
}

struct NetworkService: NetworkServiceType {

    private let urlSession: URLSession
    private let decoder: JSONDecoderType

    init(urlSession: URLSession = .shared, decoder: JSONDecoderType) {
        self.urlSession = urlSession
        self.decoder = decoder
    }

    func request<T: Decodable>(_ request: RequestType, responseType: T.Type) async throws -> T {
        do {
            let request = try request.makeRequest()
            let (data, response) = try await urlSession.data(for: request)

            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw NetworkError.invalidResponse
            }
            
            guard let decodedResponse = try? decoder.decode(responseType, from: data) else {
                throw NetworkError.decodingError
            }

            return decodedResponse
        } catch {
            throw error
        }
    }
}
