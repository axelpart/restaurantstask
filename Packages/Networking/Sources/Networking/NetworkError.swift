import Foundation

/// An error type returned when network request fails.
public enum NetworkError: Error {
    /// Failed to create a valid `URL`.
    case invalidURL
    /// Response validation failed.
    case invalidResponse
    /// Failed to decode the response type.
    case decodingError
}
