import Foundation

/// HTTP method types
public enum HTTPMethod: String {
    /// `GET` method.
    case get = "GET"
}
