import Foundation

/// A struct representing an endpoint.
public struct Endpoint: Equatable {
    /// The host requesting from.
    private let host: String
    /// The path requesting from.
    private let path: String
    /// A list of items to attach as query parameters.
    private let queryItems: [URLQueryItem]

    public init(
        host: String,
        path: String,
        queryItems: [URLQueryItem]
    ) {
        self.host = host
        self.path = path
        self.queryItems = queryItems
    }

    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = host
        components.path = path
        components.queryItems = queryItems.isEmpty ? nil : queryItems
        return components.url
    }
}
