import Foundation

/// Should be used to build the `NetworkService`
public struct NetworkServiceFactory {

    /**
     This function builds the network service.

     - Parameter decoder: A `JSONDecoderType`. Defaults to `JSONDecoder`.
     - Returns: The `NetworkServiceType`.
    */
    public static func make(
        decoder: JSONDecoderType = JSONDecoder()
    ) -> NetworkServiceType {
        NetworkService(decoder: decoder)
    }
}
