import Foundation

/// A type used to construct the request.
public protocol RequestType {
    /// Returns the `HTTPMethod` type.
    var httpMethod: HTTPMethod { get }
    /// Returns the `Endpoint` type.
    var endpoint: Endpoint { get }
}

extension RequestType {
    func makeRequest() throws -> URLRequest {
        guard let url = endpoint.url else {
            throw NetworkError.invalidURL
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        return urlRequest
    }
}
