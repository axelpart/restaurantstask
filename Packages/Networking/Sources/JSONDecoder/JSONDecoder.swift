import Foundation

/// A `JSONDecoder` type that wraps system `open` method.
public protocol JSONDecoderType {
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T: Decodable
}

extension JSONDecoder: JSONDecoderType { }
