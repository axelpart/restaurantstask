// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Networking",
    platforms: [
        .iOS(.v13),
        .macOS(.v12)
    ],
    products: [
        .library(
            name: "Networking",
            targets: ["Networking"]),
        .library(
            name: "NetworkingTestsSupport",
            targets: ["NetworkingTestsSupport"])
    ],
    targets: [
        .target(
            name: "Networking",
            dependencies: [],
            path: "Sources"),
        .target(
            name: "NetworkingTestsSupport",
            dependencies: ["Networking"],
            path: "TestsSupport"),
        .testTarget(
            name: "NetworkingTests",
            dependencies: ["Networking", "NetworkingTestsSupport"]),
    ]
)
