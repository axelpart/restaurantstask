import XCTest
import MapKit
import Combine
@testable import Domain

final class LocationsRepositoryTests: XCTestCase {

    var sut: LocationsRepository!

    var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        try super.setUpWithError()

        cancellables = []
        sut = LocationsRepository()
    }

    override func tearDownWithError() throws {
        cancellables = nil
        sut = nil
        try super.tearDownWithError()
    }

    func testTimerEmitsLocation() {
        // Given
        var expectedLocation: CLLocation?
        
        // When
        sut.locationPublisher(every: 0)
            .sink { value in
                expectedLocation = value
            }.store(in: &cancellables)

        // Then
        XCTAssertEqual(expectedLocation?.coordinate.latitude, 60.17018700)
        XCTAssertEqual(expectedLocation?.coordinate.longitude, 24.93059900)
    }
}
