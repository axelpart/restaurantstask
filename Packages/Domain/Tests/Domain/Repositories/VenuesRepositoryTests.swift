import XCTest
import MapKit
import Combine
@testable import DomainTestsSupport
@testable import StorageTestsSupport
@testable import Domain

final class VenuesRepositoryTests: XCTestCase {

    var restaurantsNetworkService: RestaurantsNetworkServiceTypeMock!
    var storage: StorageServiceTypeMock!
    var sut: VenuesRepository!

    var cancellables: Set<AnyCancellable>!
    var receivedValue: [Venue]!

    override func setUpWithError() throws {
        try super.setUpWithError()

        restaurantsNetworkService = RestaurantsNetworkServiceTypeMock()
        storage = StorageServiceTypeMock()
        storage.getTypeReturnValue = Set<String>()
        cancellables = []
        sut = VenuesRepository(
            restaurantsNetworkService: restaurantsNetworkService,
            storage: storage
        )
    }

    override func tearDownWithError() throws {
        restaurantsNetworkService = nil
        storage = nil
        cancellables = nil
        receivedValue = nil
        sut = nil
        try super.tearDownWithError()
    }

    // MARK: - Fetch data

    func testFetchVenuesReturnsSuccessfully() {
        // Given
        let location = CLLocation()
        let expectation = expectation(description: #function)
        restaurantsNetworkService.fetchRestaurantsLatLongReturnValue = [.fixture(
            itemType: .allRestaurants(
                [.fixture(venue: .fixture(location: location)), .fixture(venue: .fixture(location: location))]
            ))
        ]

        sut.venuesPublisher.sink { value in
            self.receivedValue = value
            expectation.fulfill()
        }.store(in: &cancellables)

        // When
        sut.fetchVenues(for: CLLocation()).sink()

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertEqual(receivedValue.count, 2)
        XCTAssertEqual(receivedValue, [.fixture(location: location), .fixture(location: location)])
    }

    func testFetchVenuesReturnsSuccessfullyOnlyFifteenVenues() {
        // Given
        let expectation = expectation(description: #function)
        restaurantsNetworkService.fetchRestaurantsLatLongReturnValue = makeAllRestaurantsRespone(count: 30)

        sut.venuesPublisher.sink { value in
            self.receivedValue = value
            expectation.fulfill()
        }.store(in: &cancellables)

        // When
        sut.fetchVenues(for: CLLocation()).sink()

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertEqual(receivedValue.count, 15)
    }

    func testFetchVenuesThrowsError() {
        // Given
        var expectedError: Error?
        restaurantsNetworkService.fetchRestaurantsLatLongThrowableError = ErrorFixture.fake
        let expectation = expectation(description: #function)

        // When
        sut.fetchVenues(for: CLLocation())
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    expectedError = error
                    expectation.fulfill()
                case .finished:
                    break
                }
        }, receiveValue: { _ in })
        .store(in: &cancellables)

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertEqual(expectedError?.localizedDescription, ErrorFixture.fake.localizedDescription)
    }

    func testFetchVenuesReturnsWrongItemType() {
        // Given
        let expectation = expectation(description: #function)
        restaurantsNetworkService.fetchRestaurantsLatLongReturnValue = [.fixture(itemType: .categories)]

        sut.venuesPublisher.sink { value in
            self.receivedValue = value
            expectation.fulfill()
        }.store(in: &cancellables)

        // When
        sut.fetchVenues(for: CLLocation()).sink()

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertTrue(receivedValue.isEmpty)
    }

    // MARK: - Favorites

    func testToggleVenueWillEmitAnUpdatedValueAndSyncStorage() {
        // Given
        let location = CLLocation()
        let expectation = expectation(description: #function)
        restaurantsNetworkService.fetchRestaurantsLatLongReturnValue = makeAllRestaurantsRespone(location: location)

        sut.venuesPublisher.sink { value in
            self.receivedValue = value
            expectation.fulfill()
        }.store(in: &cancellables)

        // When
        sut.toggleFavoriteVenue(with: "0")
        sut.fetchVenues(for: CLLocation()).sink()

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertTrue(storage.saveDataCalled)
        XCTAssertEqual(receivedValue, [.fixture(id: "0", isFavorited: true, location: location), .fixture(id: "1", location: location)])
    }

    func testToggleVenueRemoveValueAndSyncStorage() {
        // Given
        let location = CLLocation()
        let expectation = expectation(description: #function)

        storage.getTypeReturnValue = Set<String>(["0", "1"])
        restaurantsNetworkService.fetchRestaurantsLatLongReturnValue = makeAllRestaurantsRespone(location: location)
        sut = VenuesRepository(restaurantsNetworkService: restaurantsNetworkService, storage: storage)

        sut.venuesPublisher.sink { value in
            self.receivedValue = value
            expectation.fulfill()
        }.store(in: &cancellables)

        // When
        sut.toggleFavoriteVenue(with: "0")
        sut.fetchVenues(for: CLLocation()).sink()

        // Then
        waitForExpectations(timeout: 2)
        XCTAssertTrue(storage.saveDataCalled)
        XCTAssertEqual(receivedValue, [.fixture(id: "0", isFavorited: false, location: location), .fixture(id: "1", isFavorited: true, location: location)])
    }

    // MARK: - Helper

    private func makeAllRestaurantsRespone(
        count: Int = 1,
        location: CLLocation = CLLocation())
    -> [SectionResponse] {
        let venues: [VenueResponse] = (0...count).map { .fixture(id: String($0), location: location) }
        let res: [RestaurantResponse] = venues.map { .fixture(venue: $0) }
        return [.fixture(itemType: .allRestaurants(res))]
    }
}
