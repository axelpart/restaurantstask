import XCTest
import NetworkingTestsSupport
@testable import DomainTestsSupport
@testable import Domain

final class RestaurantsNetworkServiceTests: XCTestCase {
    var networkService: NetworkServiceTypeMock!
    var sut: RestaurantsNetworkService!

    override func setUpWithError() throws {
        try super.setUpWithError()

        networkService = NetworkServiceTypeMock()
        sut = RestaurantsNetworkService(networkService: networkService)
    }

    override func tearDownWithError() throws {
        networkService = nil
        sut = nil
        try super.tearDownWithError()
    }

    func testFetchRestaurantsSuccessfully() async throws {
        // Given
        networkService.requestResponseTypeReturnValue = RestaurantsResponse.fixture(sections: [.fixture()])

        // When
        let response = try await sut.fetchRestaurants(lat: 0, long: 0)

        // Then
        XCTAssertTrue(networkService.requestResponseTypeCalled)
        XCTAssertEqual(response.count, 1)
    }

    func testFetchRestaurantsFailure() async {
        // Given
        networkService.requestResponseTypeThrowableError = ErrorFixture.fake
        var expectedError: RestaurantsNetworkService.Error?

        // When
        do {
            _ = try await sut.fetchRestaurants(lat: 0, long: 0)
        } catch {
            expectedError = error as? RestaurantsNetworkService.Error
        }

        // Then
        XCTAssertFalse(networkService.requestResponseTypeCalled)
        XCTAssertEqual(expectedError, .generic(
            "The operation couldn’t be completed. (NetworkingTestsSupport.ErrorFixture error 0.)")
        )
    }

    // MARK: - Request
    // TODO: Move to a seperate file

    func testRequestEndpoint() {
        // Given
        let latitude = "12.34567"
        let longitude = "98.76543"

        // When
        let request = RestaurantsRequest(lat: latitude, long: longitude)

        // Then
        XCTAssertEqual(request.httpMethod, .get)
        XCTAssertEqual(request.endpoint, .restaurantsPage(lat: latitude, long: longitude))
    }
}
