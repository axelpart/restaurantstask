import XCTest
import MapKit
@testable import DomainTestsSupport
@testable import Domain

final class RestaurantsResponseTests: XCTestCase {

    var sut: RestaurantsResponse!

    override func setUpWithError() throws {
        try super.setUpWithError()

        sut = try MockJSONParser.mockDecoded(fileName: "RestaurantsPage")
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testDecodedSectionTitles() {
        // Then
        XCTAssertEqual(sut.sections.count, 2)
        XCTAssertEqual(sut.sections.first?.title.rawValue, "Browse categories")
        XCTAssertEqual(sut.sections.last?.title.rawValue, "All restaurants")
    }

    func testDecodedSectionItems() {
        // Then
        XCTAssertEqual(sut.sections.count, 2)
        XCTAssertEqual(sut.sections.first?.itemType, .categories)

        // TODO: Check if CLLocation Equatable conformance because it has timezone.
//        XCTAssertEqual(sut.sections.last?.itemType, .allRestaurants([.init(venue: .init(
//            id: "5ae6013cf78b5a000bb64022",
//            name: "McDonald's Helsinki Kamppi",
//            description: "I'm lovin' it.",
//            location: CLLocation(latitude: 60.16896328076366, longitude: 24.930145740509033)),
//            image: .init(urlString: "https://discovery-cdn.wolt.com/categories/fd1d18e0-c5a8-11ea-8a78-822e244794a0_a1852dc9_4afb_4877_9659_793adcb0f87b.jpg-md")),
//            .init(venue: .init(
//                id: "630723a28b2af6d016acbd64",
//                name: "KotKot Punavuori",
//                description: "Legendary fried chicken sandwich\n",
//                location: CLLocation(latitude: 60.16268979999999, longitude: 24.9358833)),
//                  image: .init(urlString: "https://discovery-cdn.wolt.com/categories/fd1d18e0-c5a8-11ea-8a78-822e244794a0_a1852dc9_4afb_4877_9659_793adcb0f87b.jpg-md"))])
//        )
    }

    func testVenuesDecoding() {
        // Given
        let restaurantResponse = getDecodedVenues(sut.sections.last!.itemType)

        // When
        let venue = restaurantResponse.first?.venue

        // Then
        XCTAssertEqual(restaurantResponse.count, 2)
        XCTAssertEqual(venue?.id, "5ae6013cf78b5a000bb64022")
        XCTAssertEqual(venue?.name, "McDonald\'s Helsinki Kamppi")
        XCTAssertEqual(venue?.location.coordinate.latitude, 60.16896328076366)
        XCTAssertEqual(venue?.location.coordinate.longitude, 24.930145740509033)
    }

    // MARK: - Private helper

    private func getDecodedVenues(_ itemType: SectionResponse.ItemType) -> [RestaurantResponse] {
        switch itemType {
        case .allRestaurants(let items):
            return items
        case .categories:
            return []
        }
    }
}
