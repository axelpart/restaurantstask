import XCTest
@testable import Networking
@testable import Domain

final class RestaurantsPageEndpointTests: XCTestCase {

    func testEndpointWithCoordinates() {
        // When
        let endPoint = Endpoint.restaurantsPage(lat: "32.12345", long: "21.9876543")

        // Then
        XCTAssertEqual(
            endPoint.url,
            URL(string: "https://restaurant-api.wolt.com/v1/pages/restaurants?lat=32.12345&lon=21.9876543"))
    }

}
