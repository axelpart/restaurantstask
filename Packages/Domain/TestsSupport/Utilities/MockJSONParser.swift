import Foundation
@testable import Domain

private enum Constant {
    static let fileExtension = "json"
}

struct MockJSONParser {
    enum Error: Swift.Error {
        case failedToLoadData
        case failedToDecode
    }

    static func mockDecoded<T: Decodable>(fileName: String, type: String = Constant.fileExtension) throws -> T {

        guard let url = Bundle.module.url(forResource: fileName, withExtension: type),
              let data = try? Data(contentsOf: url)
        else {
            throw Error.failedToLoadData
        }

        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            throw Error.failedToDecode
        }
    }
}
