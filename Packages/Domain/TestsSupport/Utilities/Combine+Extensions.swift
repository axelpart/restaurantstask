import Foundation
import Combine

extension Publisher where Output: Equatable {
    @discardableResult func sink() -> AnyCancellable {
        sink(
            receiveCompletion: { _ in },
            receiveValue: { _ in })
    }
}
