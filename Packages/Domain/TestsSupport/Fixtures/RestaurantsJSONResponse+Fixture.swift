import Foundation
@testable import Domain

extension RestaurantsResponse {
    public static func jsonFixture(
        hasConfirmation: Bool,
        hasPayerOrderID: Bool
    ) -> String {
        let confirmationString = hasConfirmation ? "\"confirmation\"" : "null"
        let hasPayerOrderIDString = hasPayerOrderID ? "46453" : "null"
        return """
            {
              "payment": {
                "id": 56598346,
                "created_at": "2020-02-24 15:48:41",
                "updated_at": "2020-02-24 15:48:46",
                "amount": "11.95",
                "currency": "USD",
                "status": "done",
                "subscription": {
                  "id": 42280924,
                  "created_at": "2020-02-24 14:38:04",
                  "updated_at": "2020-02-24 15:48:47",
                  "frequency_interval": 1,
                  "frequency_unit": "month",
                  "next_payment_at": "2020-02-24 15:53:41",
                  "amount": "11.95",
                  "currency": "USD",
                  "status": "active",
                  "payer": {
                    "id": 47900263,
                    "created_at": "2020-02-24 14:38:04",
                    "updated_at": "2020-02-24 14:38:05",
                    "email": "test17@test.lt",
                    "user_id": 2329653,
                    "order_id": 47644964
                  }
                },
                "payer": {
                  "id": 47900263,
                  "created_at": "2020-02-24 14:38:04",
                  "updated_at": "2020-02-24 14:38:05",
                  "email": "test17@test.lt",
                  "user_id": 2329653,
                  "order_id": \(hasPayerOrderIDString)
                },
                "confirmation": \(confirmationString),
                "payment_errors": [],
                "amounts": [
                  {
                    "id": 85336024,
                    "created_at": "2020-02-24 15:48:46",
                    "updated_at": "2020-02-24 15:48:46",
                    "payment_id": 56598346,
                    "amount": 11.95,
                    "currency": "USD",
                    "rate": 1
                  },
                  {
                    "id": 85336027,
                    "created_at": "2020-02-24 15:48:46",
                    "updated_at": "2020-02-24 15:48:46",
                    "payment_id": 56598346,
                    "amount": 11.0253,
                    "currency": "EUR",
                    "rate": 1.08387
                  }
                ]
              }
            }
        """
    }
}
