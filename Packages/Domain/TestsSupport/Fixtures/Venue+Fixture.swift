import Foundation
import MapKit
@testable import Domain

extension Venue {
    public static func fixture(
        id: String = "",
        title: String = "",
        description: String = "",
        isFavorited: Bool = false,
        location: CLLocation = CLLocation(),
        imageURL: URL? = nil
    ) -> Self {
        .init(
            id: id,
            title: title,
            description: description,
            isFavorited: isFavorited,
            location: location,
            imageURL: imageURL
        )
    }
}
