import Foundation
import MapKit
@testable import Domain

extension RestaurantsResponse {
    static func fixture(sections: [SectionResponse] = []) -> Self {
        .init(sections: sections)
    }
}

extension SectionResponse {
    static func fixture(
        itemType: ItemType = .allRestaurants([]),
        title: Title = .allRestaurants
    ) -> Self {
        .init(itemType: itemType, title: title)
    }
}

extension RestaurantResponse {
    static func fixture(
        venue: VenueResponse = .fixture(),
        image: ImageResponse = .fixture()
    ) -> Self {
        .init(venue: venue, image: image)
    }
}

extension VenueResponse {
    static func fixture(
        id: String = "",
        name: String = "",
        description: String = "",
        location: CLLocation = CLLocation()
    ) -> Self {
        .init(
            id: id,
            name: name,
            description: description,
            location: location
        )
    }
}

extension ImageResponse {
    static func fixture(urlString: String = "") -> Self {
        .init(urlString: urlString)
    }
}
