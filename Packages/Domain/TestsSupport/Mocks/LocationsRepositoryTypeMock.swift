import Foundation
import Combine
import MapKit
@testable import Domain

final public class LocationsRepositoryTypeMock: LocationsRepositoryType {

    public init() { }

    public var locationPublisherEveryCallsCount = 0
    public var locationPublisherEveryCalled: Bool {
        return locationPublisherEveryCallsCount > 0
    }
    public var locationPublisherEveryReturnValue: AnyPublisher<CLLocation, Never>!

    public func locationPublisher(every interval: TimeInterval) -> AnyPublisher<CLLocation, Never> {
        locationPublisherEveryCallsCount += 1
        return locationPublisherEveryReturnValue
    }
}
