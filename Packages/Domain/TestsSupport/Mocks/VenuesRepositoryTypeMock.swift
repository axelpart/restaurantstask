import Foundation
import Combine
import MapKit
@testable import Domain

final public class VenuesRepositoryTypeMock: VenuesRepositoryType {

    public var venuesPublisher: AnyPublisher<[Venue], Never> {
        get { return underlyingVenuesPublisher }
        set(value) { underlyingVenuesPublisher = value }
    }
    public var underlyingVenuesPublisher: AnyPublisher<[Venue], Never>!

    public init() { }

    public var fetchVenuesForCallsCount = 0
    public var fetchVenuesForCalled: Bool {
        return fetchVenuesForCallsCount > 0
    }

    public var fetchVenuesForReturnValue: AnyPublisher<Never, Error>!

    public func fetchVenues(for location: CLLocation) -> AnyPublisher<Never, Error> {
        fetchVenuesForCallsCount += 1
        return fetchVenuesForReturnValue
    }

    public var toggleFavoriteVenueWithCallsCount = 0
    public var toggleFavoriteVenueWithCalled: Bool {
        return toggleFavoriteVenueWithCallsCount > 0
    }

    public func toggleFavoriteVenue(with id: String) {
        toggleFavoriteVenueWithCallsCount += 1
    }
}
