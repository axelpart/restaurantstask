import Foundation
@testable import Domain

final class RestaurantsNetworkServiceTypeMock: RestaurantsNetworkServiceType {

    var fetchRestaurantsLatLongThrowableError: Error?
    var fetchRestaurantsLatLongCallsCount = 0
    var fetchRestaurantsLatLongCalled: Bool {
        return fetchRestaurantsLatLongCallsCount > 0
    }

    var fetchRestaurantsLatLongReturnValue: [SectionResponse]!

    func fetchRestaurants(lat: Double, long: Double) async throws -> [SectionResponse] {
        if let error = fetchRestaurantsLatLongThrowableError {
            throw error
        }
        fetchRestaurantsLatLongCallsCount += 1
        return fetchRestaurantsLatLongReturnValue
    }
}
