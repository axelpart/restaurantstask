import Foundation
import Networking

extension Endpoint {
    static func restaurantsPage(lat: String, long: String) -> Endpoint {
        Endpoint(
            host: "restaurant-api.wolt.com",
            path: "/v1/pages/restaurants",
            queryItems: [
                URLQueryItem(name: "lat", value: lat),
                URLQueryItem(name: "lon", value: long)
            ])
    }
}
