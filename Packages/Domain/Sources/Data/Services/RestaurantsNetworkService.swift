import Foundation
import Networking
import MapKit

/// A service used to fetch a page with restaurant items.
protocol RestaurantsNetworkServiceType {
    /// Fetches a page of sections based on latitude and longitude defined.
    /// - Parameter lat: `Double` location latitude.
    /// - Parameter long: `Double` location longitude.
    /// - Returns: An array of sections.
    /// - Throws: A generic `Error` if failed.
    func fetchRestaurants(lat: Double, long: Double) async throws -> [SectionResponse]
}

struct RestaurantsNetworkService: RestaurantsNetworkServiceType {
    private let networkService: NetworkServiceType

    enum Error: Swift.Error, Equatable {
        case generic(String)
    }

    init(networkService: NetworkServiceType = NetworkServiceFactory.make()) {
        self.networkService = networkService
    }

    func fetchRestaurants(lat: Double, long: Double) async throws -> [SectionResponse] {
        let request = RestaurantsRequest(lat: lat.description, long: long.description)
        do {
            let page = try await networkService.request(request, responseType: RestaurantsResponse.self)
            return page.sections
        } catch  {
            throw RestaurantsNetworkService.Error.generic(
                error.localizedDescription
            )
        }
    }
}

// MARK: - Request

struct RestaurantsRequest: RequestType {
    private let lat: String
    private let long: String

    let httpMethod: HTTPMethod = .get
    var endpoint: Endpoint {
        return Endpoint.restaurantsPage(lat: lat, long: long)
    }

    init(lat: String, long: String) {
        self.lat = lat
        self.long = long
    }
}
