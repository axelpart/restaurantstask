import Foundation
import MapKit

struct VenueResponse: Decodable, Equatable {
    let id: String
    let name: String
    let description: String
    let location: CLLocation
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description = "short_description"
        case location
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        description = try container.decode(String.self, forKey: .description)
        let coordinates = try container.decode([Double].self, forKey: .location)
        location = CLLocation(latitude: coordinates[1], longitude: coordinates[0])
    }

    init(
        id: String,
        name: String,
        description: String,
        location: CLLocation
    ) {
        self.id = id
        self.name = name
        self.description = description
        self.location = location
    }
}
