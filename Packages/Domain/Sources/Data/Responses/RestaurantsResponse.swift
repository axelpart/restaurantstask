import Foundation

struct RestaurantsResponse: Decodable {
    let sections: [SectionResponse]
}

struct SectionResponse: Decodable {
    let itemType: ItemType
    let title: Title

    enum CodingKeys: String, CodingKey {
        case title
        case items
    }

    enum Title: String, Decodable {
        case categories = "Browse categories"
        case allRestaurants = "All restaurants"
    }

    enum ItemType: Decodable, Equatable {
        case categories
        case allRestaurants([RestaurantResponse])
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(Title.self, forKey: .title)

        switch title {
        case .categories:
            itemType = .categories
        case .allRestaurants:
            let venues = try container.decode([RestaurantResponse].self, forKey: .items)
            itemType = .allRestaurants(venues)
        }
    }

    init(itemType: ItemType, title: Title) {
        self.itemType = itemType
        self.title = title
    }
}

struct RestaurantResponse: Decodable, Equatable {
    let venue: VenueResponse
    let image: ImageResponse
}
