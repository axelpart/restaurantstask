import Foundation

struct ImageResponse: Decodable, Equatable {
    let urlString: String

    enum CodingKeys: String, CodingKey {
        case urlString = "url"
    }
}
