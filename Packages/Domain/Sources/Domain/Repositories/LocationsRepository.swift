import Foundation
import Combine
import MapKit

/// A repository used to provide a location at a given interval.
public protocol LocationsRepositoryType {
    /// Returns a publisher that emits location values based on the time interval defined.
    /// - Parameter TimeInterval: The time interval on which a `Timer` will publish events.
    /// - Returns: A publisher that repeatedly emits a `CLLocation`.
    func locationPublisher(every interval: TimeInterval) -> AnyPublisher<CLLocation, Never>
}

struct LocationsRepository: LocationsRepositoryType {
    private var cancellables = Set<AnyCancellable>()

    func locationPublisher(every interval: TimeInterval) -> AnyPublisher<CLLocation, Never>{
        timerCounterPublisher(with: interval)
            .compactMap { self.locations[$0] }
            .eraseToAnyPublisher()
    }

    // MARK: - Timer Publisher

    private func timerCounterPublisher(with interval: TimeInterval) -> AnyPublisher<Int, Never> {
        var count = 0
        return Timer // TODO: Add a protocol to be able to mock better for testing
            .publish(every: interval, on: .main, in: .default)
            .autoconnect()
            .map { _ in
                self.countUp(count: &count)
                return count
            }
            .prepend(count)
            .eraseToAnyPublisher()
    }

    private func countUp(count: inout Int) {
        count += 1
        if count == locations.count {
            count = 0
        }
    }

    // MARK: - Location coordinates

    private let coordinates = [
        60.170187, 24.930599,
        60.169418, 24.931618,
        60.169818, 24.932906,
        60.170005, 24.935105,
        60.169108, 24.936210,
        60.168355, 24.934869,
        60.167560, 24.932562,
        60.168254, 24.931532,
        60.169012, 24.930341,
        60.170085, 24.929569
    ]

    private var locations: [CLLocation] {
        stride(from: 0, to: coordinates.count, by: 2)
            .map {
                let latitude: Double = coordinates[$0]
                let longitude: Double = coordinates[$0 + 1]
                return CLLocation(latitude: latitude, longitude: longitude)
            }
    }
}
