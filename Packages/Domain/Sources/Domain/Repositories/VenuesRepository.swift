import Foundation
import Combine
import Storage
import MapKit

/// A repository used to fetch restaurant items and map them to a list of venues.
public protocol VenuesRepositoryType {
    /// Returns a publisher that emits venue values.
    /// - Returns: A publisher that emits an array of `Venue`.
    var venuesPublisher: AnyPublisher<[Venue], Never> { get }

    /// Fetches a page of sections based on location defined.
    /// - Parameter location: A `CLLocation`.
    /// - Returns: A publisher that emits an `Error` if failed.
    func fetchVenues(for location: CLLocation) -> AnyPublisher<Never, Error>

    /// Updates `Venue` favorite status and updates storage with most recent values.
    /// - Parameter id: A `Venue` id.
    func toggleFavoriteVenue(with id: String)
}

final class VenuesRepository: VenuesRepositoryType {
    private let restaurantsNetworkService: RestaurantsNetworkServiceType
    private let storage: StorageServiceType

    private let restaurantsSubject = PassthroughSubject<[RestaurantResponse], Never>()
    private let favoritesSubject: CurrentValueSubject<Set<String>, Never>
    private var cancellable: AnyCancellable?

    init(restaurantsNetworkService: RestaurantsNetworkServiceType = RestaurantsNetworkService(),
         storage: StorageServiceType = UserDefaultsStorageFactory.userDefaults(key: "favorite_venues")
    ) {
        self.restaurantsNetworkService = restaurantsNetworkService
        self.storage = storage

        let favorites = try? storage.get(Set<String>.self)
        favoritesSubject = CurrentValueSubject(favorites ?? [])
    }

    var venuesPublisher: AnyPublisher<[Venue], Never> {
        restaurantsSubject
            .combineLatest(favoritesSubject.eraseToAnyPublisher())
            .map(toVenue)
            .eraseToAnyPublisher()
    }

    func fetchVenues(for location: CLLocation) -> AnyPublisher<Never, Error> {
        Future { [restaurantsNetworkService, self] promise in
            Task {
                do {
                    let items = try await restaurantsNetworkService
                        .fetchRestaurants(lat: location.coordinate.latitude, long: location.coordinate.longitude)
                        .map(\.itemType)
                        .flatMap(getVenues)
                        .sorted(by: { item1, item2 in
                            return item1.venue.location.distance(from: location) < item2.venue.location.distance(from: location)
                        })
                        .prefix(15)
                    self.restaurantsSubject.send(Array(items))
                } catch {
                    promise(.failure(error))
                }
            }
        }.eraseToAnyPublisher()
    }

    func toggleFavoriteVenue(with id: String) {
       cancellable = favoritesSubject
            .first()
            .map { value in
                value.contains(where: { $0 == id })
            }
            .sink { [weak self] in
                self?.updateFavoriteItem(id, shouldRemove: $0)
            }
    }

    private func updateFavoriteItem(_ id: String, shouldRemove: Bool) {
        if shouldRemove {
            favoritesSubject.value.remove(id)
        } else {
            favoritesSubject.value.insert(id)
        }
        updateStorage()
        favoritesSubject.send(favoritesSubject.value)
    }

    private func updateStorage() {
        try? storage.save(favoritesSubject.value)
    }
}

// MARK: - Private

private func toVenue(venues: [RestaurantResponse], favorites: Set<String>) -> [Venue] {
    return venues.map { item in
        Venue(
            id: item.venue.id,
            title: item.venue.name,
            description: item.venue.description,
            isFavorited: favorites.contains(where: { $0 == item.venue.id }),
            location: item.venue.location,
            imageURL: URL(string: item.image.urlString)
        )
    }
}

private func getVenues(from type: SectionResponse.ItemType) -> [RestaurantResponse] {
    switch type {
    case .allRestaurants(let items):
        return items
    case .categories:
        return []
    }
}
