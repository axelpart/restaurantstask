import Foundation

/// Should be used to build the `VenuesRepository`
public struct VenuesRepositoryFactory {
    /**
     This function builds the location repository.

     - Returns: The `VenuesRepositoryType`.
    */
    public static func make() -> VenuesRepositoryType {
        return VenuesRepository()
    }
}
