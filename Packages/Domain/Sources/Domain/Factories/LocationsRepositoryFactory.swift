import Foundation

/// Should be used to build the `LocationsRepository`
public struct LocationsRepositoryFactory {
    /**
     This function builds the location repository.

     - Returns: The `LocationsRepositoryType`.
    */
    public static func make() -> LocationsRepositoryType {
        return LocationsRepository()
    }
}
