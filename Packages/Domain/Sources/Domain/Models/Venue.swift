import Foundation
import MapKit

public struct Venue: Identifiable, Equatable {
    public let id: String
    public let title: String
    public let description: String
    public let isFavorited: Bool
    public let location: CLLocation
    public let imageURL: URL?

    public init(
        id: String,
        title: String,
        description: String,
        isFavorited: Bool,
        location: CLLocation,
        imageURL: URL?
    ) {
        self.id = id
        self.title = title
        self.description = description
        self.isFavorited = isFavorited
        self.location = location
        self.imageURL = imageURL
    }
}
