// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Domain",
    platforms: [
        .iOS(.v13),
        .macOS(.v12)
    ],
    products: [
        .library(
            name: "Domain",
            targets: ["Domain"]),
        .library(
            name: "DomainTestsSupport",
            targets: ["DomainTestsSupport"])
    ],
    dependencies: [
        .package(name: "Networking", path: "../Networking"),
        .package(name: "Storage", path: "../Storage"),
    ],
    targets: [
        .target(
            name: "Domain",
            dependencies: ["Networking", "Storage"],
            path: "Sources"),
        .target(
            name: "DomainTestsSupport",
            dependencies: ["Domain"],
            path: "TestsSupport",
            resources: [
                .copy("Resources/RestaurantsPage.json")
            ]),
        .testTarget(
            name: "DomainTests",
            dependencies: [
                "Domain",
                "DomainTestsSupport",
                .product(name: "NetworkingTestsSupport", package: "Networking"),
                .product(name: "StorageTestsSupport", package: "Storage")
            ],
            path: "Tests"),
    ]
)
