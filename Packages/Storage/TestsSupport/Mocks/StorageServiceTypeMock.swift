import Foundation
@testable import Storage

public final class StorageServiceTypeMock: StorageServiceType {

    public init() { }

    public var getTypeThrowableError: Error?
    public var getTypeCallsCount = 0
    public var getTypeCalled: Bool {
        return getTypeCallsCount > 0
    }

    public var getTypeReturnValue: Any!

    public func get<T: Decodable>(_ type: T.Type) throws -> T {
        if let error = getTypeThrowableError {
            throw error
        }
        getTypeCallsCount += 1
        return getTypeReturnValue as! T
    }

    public var saveDataThrowableError: Error?
    public var saveDataCallsCount = 0
    public var saveDataCalled: Bool {
        return saveDataCallsCount > 0
    }

    public func save<T: Encodable>(_ data: T) throws {
        if let error = saveDataThrowableError {
            throw error
        }
        saveDataCallsCount += 1
    }

    public var removeCallsCount = 0
    public var removeCalled: Bool {
        return removeCallsCount > 0
    }

    public func remove() {
        removeCallsCount += 1
    }
}
