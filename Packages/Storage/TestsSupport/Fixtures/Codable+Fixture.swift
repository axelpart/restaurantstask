import Foundation

struct Foo: Codable {
    let id: Int
}

struct Bar: Codable {
    let id: Int
    let name: String
}
