import Foundation

struct ThrowableObject: Encodable {
    func encode(to encoder: Encoder) throws {
        throw ErrorFixture.fake
    }
}
