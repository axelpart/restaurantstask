import XCTest
@testable import StorageTestsSupport
@testable import Storage

final class UserDefaultsStorageTests: XCTestCase {
    var userDefaults: UserDefaults!
    var sut: UserDefaultsStorage!

    // MARK: - Life cycle

    override func setUpWithError() throws {
        try super.setUpWithError()

        userDefaults = UserDefaults(suiteName: #file)
        sut = UserDefaultsStorage(userDefaults: userDefaults, key: "storage_tests")
    }

    override func tearDownWithError() throws {
        userDefaults.removePersistentDomain(forName: #file)
        userDefaults = nil
        sut = nil
        try super.tearDownWithError()
    }

    // MARK: - Get item

    func testGetThrowsNoDataErrorItemDoesNotExist() {
        // Given
        var expectedError: UserDefaultsStorage.Error?

        // When
        do {
            _ = try sut.get(Foo.self)
        } catch {
            expectedError = error as? UserDefaultsStorage.Error
        }

        // Then
        XCTAssertEqual(expectedError, .itemNotFound)
    }

    func testGetThrowsDecodingErrorWhenTypeDoesNotExist() throws {
        // Given
        var expectedError: UserDefaultsStorage.Error?
        try sut.save(Foo(id: 1))

        // When
        do {
            _ = try sut.get(Bar.self)
        } catch {
            expectedError = error as? UserDefaultsStorage.Error
        }

        // Then
        XCTAssertEqual(expectedError, .failedToDecodeItem)
    }

    func testGetReturnsItemSuccessfully() throws {
        // Given
        try sut.save(Foo(id: 1))

        // When
        let result = try sut.get(Foo.self)

        // Then
        XCTAssertEqual(result.id, 1)
    }

    // MARK: - Save item

    func testSaveThrowsErrorWhenEncodingFails() {
        // Given
        var expectedError: UserDefaultsStorage.Error?

        // When
        do {
            _ = try sut.save(ThrowableObject())
        } catch {
            expectedError = error as? UserDefaultsStorage.Error
        }

        // Then
        XCTAssertEqual(expectedError, .failedToSaveItem)
    }

    func testSaveItemSuccessfully() throws {
        // Given
        try sut.save(Foo(id: 1))

        // When
        let item = try sut.get(Foo.self)

        // Then
        XCTAssertEqual(item.id, 1)
    }

    // MARK: - Remove

    func testRemoveItemSuccessfully() throws {
        // Given
        try sut.save(Foo(id: 1))
        XCTAssertEqual(try sut.get(Foo.self).id, 1)

        // When
        sut.remove()

        // Then
        XCTAssertThrowsError(try sut.get(Foo.self)) { errorThrown in
            XCTAssertEqual(errorThrown as? UserDefaultsStorage.Error, .itemNotFound)
        }
    }
}
