// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Storage",
    platforms: [
        .iOS(.v13),
        .macOS(.v12)
    ],
    products: [
        .library(
            name: "Storage",
            targets: ["Storage"]),
        .library(
            name: "StorageTestsSupport",
            targets: ["StorageTestsSupport"])
    ],
    targets: [
        .target(
            name: "Storage",
            path: "Sources"),
        .target(
            name: "StorageTestsSupport",
            dependencies: ["Storage"],
            path: "TestsSupport"),
        .testTarget(
            name: "StorageTests",
            dependencies: ["Storage", "StorageTestsSupport"]),
    ]
)
