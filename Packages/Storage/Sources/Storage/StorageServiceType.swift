import Foundation
import Combine

// TODO: Provide a reactive interface using Combine

/// Generic storage using `Codable`.
public protocol StorageServiceType {

    /// Returns a stored item.
    /// - Returns: An item with the defined generic `Type`.
    /// - Throws: An `Error` when item not found or (item) decoding fails.
    func get<T: Decodable>(_ type: T.Type) throws -> T

    /// Saves the given item defined.
    /// - Throws: An `Error` holding succes/failure.
    func save<T: Encodable>(_ data: T) throws

    /// Removes an object if found.
    func remove()
}

struct UserDefaultsStorage: StorageServiceType {

    private let key: String
    private let userDefaults: UserDefaults

    enum Error: Swift.Error {
        case itemNotFound
        case failedToDecodeItem
        case failedToSaveItem
    }

    init(userDefaults: UserDefaults, key: String) {
        self.key = key
        self.userDefaults = userDefaults
    }

    func get<T: Decodable>(_ type: T.Type) throws -> T {
        guard let data = userDefaults.data(forKey: key) else {
            throw Error.itemNotFound
        }

        guard let item = try? JSONDecoder().decode(T.self, from: data) else {
            throw Error.failedToDecodeItem
        }

        return item
    }

    func save<T: Encodable>(_ data: T) throws {
        guard let encodedData = try? JSONEncoder().encode(data) else {
            throw Error.failedToSaveItem
        }
        userDefaults.set(encodedData, forKey: key)
    }

    func remove() {
        userDefaults.removeObject(forKey: key)
    }
}
