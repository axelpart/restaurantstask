import Foundation

/// Should be used to build the `UserDefaultsStorage`
public struct UserDefaultsStorageFactory {
    /**
     This function builds the user defaults storage.

     - Parameter userDefaults: A `UserDefaults` object. Defaults to `standard`.
     - Parameter key: A `String` used as the key.
     - Returns: The `StorageServiceType`.
    */
    public static func userDefaults(
        userDefaults: UserDefaults = .standard,
        key: String
    ) -> StorageServiceType {
        return UserDefaultsStorage(
            userDefaults: userDefaults,
            key: key
        )
    }
}
