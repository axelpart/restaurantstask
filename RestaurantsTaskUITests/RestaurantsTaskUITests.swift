import XCTest

final class RestaurantsTaskUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false

        app = XCUIApplication()
        app.launchArguments = ["uitesting"]
        app.launch()
    }

    override func tearDownWithError() throws {
        app.terminate()
        app = nil
        try super.tearDownWithError()
    }

    func testListItemsCount() {
        let collectionView = app.collectionViews.firstMatch

        sleep(5)

        XCTAssertTrue(collectionView.exists)
        XCTAssertEqual(collectionView.cells.count, 10)
    }

    func testRowElementsItems() {
        let heartIcon = app.images["favorite"]
        let title = app.staticTexts["Pupu Tennispalatsi"]
        let description = app.staticTexts["Passionate about coffee!"]

        sleep(5)

        XCTAssertTrue(title.exists)
        XCTAssertTrue(description.exists)
        XCTAssertTrue(heartIcon.exists)
    }
}
