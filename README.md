# Restaurants - Wolt

### Wolt assignment showcasing a list of restaurants given location coordinates.

### Couple of notes:

I decided to use MVVM architecture with SwiftUI for the UI part as it was an excellent opportunity to create an app from scratch. Code is seperated into packages and every package has a seperate target for test data (fixtures/ mocks).

* For networking i used URLSession with async await.
* For storage i decided to use UserDefaults because setting up `CoreData` in a swift package was not straight forward.
* Last but least i had to stop somewhere, which means you will find few **// TODO:** in the code.
