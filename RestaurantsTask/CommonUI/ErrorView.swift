import SwiftUI

struct ErrorView: View {
    let errorDescription: String
    let buttonTitle: String
    let onButtonAction: () -> Void

    var body: some View {
        VStack {
            Text("Something went wrong.")
                .font(.system(size: 24))
                .fontWeight(.bold)
            Text(errorDescription)
                .padding(.all, 4)
                .multilineTextAlignment(.center)
            if #available(iOS 15.0, *) {
                Button(buttonTitle) {
                    onButtonAction()
                }
                .buttonStyle(.borderedProminent)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(errorDescription: "Error", buttonTitle: "Try again", onButtonAction: { })
    }
}
