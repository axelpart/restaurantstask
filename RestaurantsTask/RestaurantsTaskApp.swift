//
//  RestaurantsTaskApp.swift
//  RestaurantsTask
//
//  Created by Alexandros Partonas on 3/24/23.
//

import SwiftUI
import Domain

@main
struct RestaurantsTaskApp: App {
    var body: some Scene {
        WindowGroup {
            VenuesListView(
                viewModel: VenuesListViewModel(
                    venuesRepository: VenuesRepositoryFactory.make(),
                    locationsRepository: LocationsRepositoryFactory.make()
                )
            )
        }
    }
}
