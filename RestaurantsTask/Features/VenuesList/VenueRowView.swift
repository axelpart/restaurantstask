import SwiftUI
import MapKit
import Domain

struct VenueRowView: View {
    let venue: Venue
    let onFavoriteAction: (String) -> ()

    var body: some View {
        HStack {
            AsyncImage(url: venue.imageURL, transaction: Transaction(animation: .easeIn)) { phase in
                switch phase {
                case .empty:
                    Color.purple.opacity(0.1)
                case .success(let image):
                    image
                        .resizable()
                        .scaledToFill()
                case .failure(_):
                    Image(systemName: "exclamationmark.icloud")
                        .resizable()
                        .scaledToFit()
                @unknown default:
                    Image(systemName: "exclamationmark.icloud")
                }
            }
            .frame(width: 60, height: 60)
            .cornerRadius(10)
            VStack(alignment: .leading) {
                Text(venue.title)
                    .font(.headline)
                    .scaledToFit()
                    .lineLimit(1)
                    .padding(.bottom, 0.1)
                Text(venue.description)
                    .font(.caption)
                    .lineLimit(2)
            }.padding([.bottom, .top], 4)
            Spacer()
            Image(venue.isFavorited ? "favorite_filled" : "favorite")
                .padding()
                .onTapGesture {
                    onFavoriteAction(venue.id)
                }
        }
    }
}

struct VenueRowView_Previews: PreviewProvider {
    static var previews: some View {
        VenueRowView(venue: Venue.previewItem, onFavoriteAction: { _ in })
    }
}

private extension Venue {
    static var previewItem: Venue {
        .init(
            id: "0",
            title: "Mia Moin",
            description: "Fine Korean Cuisine",
            isFavorited: true,
            location: CLLocation(),
            imageURL: URL(string: "https://prod-wolt-venue-images-cdn.wolt.com/617645e7ea4ffbc6fcfbacc0/e3231ab0-a45a-11ec-ad76-62832a669505_cover.jpg")
        )
    }
}
