import Foundation
import Domain
import Combine

final class VenuesListViewModel<AnyScheduler: Scheduler>: ObservableObject {
    private let venuesRepository: VenuesRepositoryType
    private let locationsRepository: LocationsRepositoryType

    private var scheduler: AnyScheduler
    private var cancellables = Set<AnyCancellable>()

    @Published private(set) var state = State.idle

    enum State: Equatable {
        case idle
        case loading
        case failed(String)
        case loaded([Venue])
    }

    init(
        venuesRepository: VenuesRepositoryType,
        locationsRepository: LocationsRepositoryType,
        scheduler: AnyScheduler = DispatchQueue.main
    ) {
        self.venuesRepository = venuesRepository
        self.locationsRepository = locationsRepository
        self.scheduler = scheduler
    }

    func startObserver() {
        venuesRepository
            .venuesPublisher
            .receive(on: scheduler)
            .sink(receiveValue: { [weak self] venues in
                self?.state = .loaded(venues)
            })
            .store(in: &cancellables)
    }

    func load() {
        locationsRepository
            .locationPublisher(every: 10)
            .flatMap { [venuesRepository] location in
                venuesRepository.fetchVenues(for: location)
            }
            .handleEvents(receiveSubscription: { [weak self] _ in
                self?.state = .loading
            })
            .receive(on: scheduler)
            .sink { [weak self] result in
                switch result {
                case .failure(let error):
                    // TODO: Handle error description better.
                    self?.state = .failed(error.localizedDescription)
                case .finished:
                    break
                }
            } receiveValue: { _ in }
            .store(in: &cancellables)
    }

    func toggleFavorite(with id: String) {
        venuesRepository.toggleFavoriteVenue(with: id)
    }
}
