import SwiftUI
import Domain

struct VenuesListView: View {
    @StateObject var viewModel: VenuesListViewModel<DispatchQueue>

    var body: some View {
        switch viewModel.state {
        case .idle:
            Color
                .clear
                .onAppear(perform: {
                    viewModel.startObserver()
                    viewModel.load()
                }
            )
        case .loading:
            ProgressView().scaleEffect(2)
        case .failed(let description):
            ErrorView(
                errorDescription: description,
                buttonTitle: "Reload",
                onButtonAction: viewModel.load)
        case .loaded(let venues):
            List(venues) {
                VenueRowView(venue: $0, onFavoriteAction: viewModel.toggleFavorite)
            }
            .listStyle(.inset)
            .transition(AnyTransition.opacity.animation(.default))
        }
    }
}

struct VenuesListView_Previews: PreviewProvider {
    static var previews: some View {
        VenuesListView(
            viewModel: VenuesListViewModel(
                venuesRepository: VenuesRepositoryFactory.make(),
                locationsRepository: LocationsRepositoryFactory.make()
            )
        )
    }
}
