import XCTest
import Combine
import MapKit
import Domain
@testable import DomainTestsSupport
@testable import RestaurantsTask

final class VenuesListViewModelTests: XCTestCase {

    var venuesRepository: VenuesRepositoryTypeMock!
    var locationsRepository: LocationsRepositoryTypeMock!

    var venuesSubject: PassthroughSubject<[Venue], Never>!
    var sut: VenuesListViewModel<ImmediateScheduler>!

    var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        try super.setUpWithError()

        venuesSubject = PassthroughSubject()
        cancellables = []
        venuesRepository = VenuesRepositoryTypeMock()
        venuesRepository.underlyingVenuesPublisher = venuesSubject.eraseToAnyPublisher()
        locationsRepository = LocationsRepositoryTypeMock()
        sut = VenuesListViewModel(
            venuesRepository: venuesRepository,
            locationsRepository: locationsRepository,
            scheduler: ImmediateScheduler.shared
        )
    }

    override func tearDownWithError() throws {
        venuesSubject = nil
        cancellables = nil
        venuesRepository = nil
        locationsRepository = nil
        sut = nil
        try super.tearDownWithError()
    }

    // MARK: - State

    func testViewStateIsLoading() {
        // Given
        sut.startObserver()
        locationsRepository.locationPublisherEveryReturnValue = Just(CLLocation()).eraseToAnyPublisher()
        venuesRepository.fetchVenuesForReturnValue = Empty().eraseToAnyPublisher()

        // When
        sut.load()

        // Then
        XCTAssertTrue(venuesRepository.fetchVenuesForCalled)
        XCTAssertEqual(sut.state, .loading)
    }

    func testViewStateLoaded() {
        // Given
        let location = CLLocation()
        sut.startObserver()

        // When
        venuesSubject.send([.fixture(location: location)])

        // Then
        XCTAssertEqual(sut.state, .loaded([.fixture(location: location)]))
    }

    func testViewStateFailure() {
        // Given
        sut.startObserver()
        locationsRepository.locationPublisherEveryReturnValue = Just(CLLocation()).eraseToAnyPublisher()
        venuesRepository.fetchVenuesForReturnValue = Fail(error: NSError(domain: "Server Unavailable", code: 503, userInfo: nil)).eraseToAnyPublisher()

        // When
        sut.load()

        // Then
        XCTAssertTrue(venuesRepository.fetchVenuesForCalled)
        XCTAssertEqual(sut.state, .failed("The operation couldn’t be completed. (Server Unavailable error 503.)"))
    }

    // MARK: - Favorites

    func testToggleFavoritesCallsRepository() {
        // When
        sut.toggleFavorite(with: "1")

        // Then
        XCTAssertTrue(venuesRepository.toggleFavoriteVenueWithCalled)
    }
}
